const HomePage = function() {

    const po = this;

    po.linkRestaurant =  function (name) {
        return by.xpath("//td/a/b[contains(text(),'" + name + "')]");
    }

    po.goToRestaurant = async function(restaurant) {
        await browser.get(browser.baseUrl + '/index.html#')
        await browser.element(po.linkRestaurant(restaurant)).click();
    }
};


module.exports = new HomePage();