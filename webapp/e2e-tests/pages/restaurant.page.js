const format = require('util').format;

const RestaurantPage = function() {

    const po = this;


    po.menuList = by.css('fm-menu-list')


    // One responsibility of a traditional Page Object
    // is to define the structure of a page the test will interact with.

    po.menuItem = function (name) {
        return by.xpath("//a/span[1][contains(text(),'" + name + "')]");
    }


    po.addItem2Order = async function (name) {
        await browser.element(po.menuItem(name)).click();
    };


};

module.exports = new RestaurantPage();