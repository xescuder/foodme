'use strict';

var chai = require('chai');
var chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);
var expect = chai.expect;

const startPage = require('../../pages/start.page');
const homePage = require('../../pages/home.page');
const restaurantPage = require('../../pages/restaurant.page');
const orderPage = require('../../pages/order.page');

var logger = require('../../logger');

module.exports = function addAlreadyExistingItem() {

    this.setDefaultTimeout(60 * 1000);


    this.Given(/^.*that I have an order containing (.*?) from restaurant (.*?)$/, async function(item, restaurant) {
        await startPage.doLogin('Xavier', 'Yeees');
        await homePage.goToRestaurant(restaurant);
        await restaurantPage.addItem2Order(item);
    });

    this.When(/^.*I add the item (.*?) from restaurant (.*?) to my order$/, async function(item, restaurant)  {
        await homePage.goToRestaurant(restaurant);
        await restaurantPage.addItem2Order(item);
    });

    this.Then(/^my order should contain (\d+) items of (.*?)$/, async function(numItems, item)  {
       var qty = await orderPage.getOrderItemQty(item);
       expect(qty).to.equal(parseInt(numItems));
    });
};
