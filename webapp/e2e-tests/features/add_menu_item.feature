Feature: Collecting menu items in my order
  As a shop visitor
  I want to collect menu items in my order
  so that I can purchase multiple menu items at once

  Scenario: Adding the same menu item to the shopping cart

    Given that I have an order containing Chicken teriyaki from restaurant Robatayaki Hachi
    When I add the item Chicken teriyaki from restaurant Robatayaki Hachi to my order
    Then my order should contain 2 items of Chicken teriyaki